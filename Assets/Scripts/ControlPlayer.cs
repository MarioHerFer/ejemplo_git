﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.EventSystems;

public class ControlPlayer : MonoBehaviour {


    Vector3 starPos;

    public float speed = 1.0f;
    bool rec, stopGo;
    List<Vector3> ps = new List<Vector3>();

    Vector3 tempPos;

    Vector2 input;

    int i = 0;
    
    private void Start() { 
        i = 0;
        starPos = transform.position;
        rec = true;
        stopGo = false;
        input = new Vector2();
    }

    void Update(){
        if(rec == true){
            Rec();
        }else{
            Go();
        }
    }

    void Rec(){
        //movimiento: WASD o flechas
        input.x = Input.GetAxis("Horizontal") * Time.deltaTime * (speed*5);
        input.y = Input.GetAxis("Vertical") * Time.deltaTime * (speed*5);
        transform.Translate(input.x , input.y , 0);

        //Rellenar List
        ps.Add(input);
        
        if(Input.GetKey("space")){
                transform.position = starPos;
                rec = false;
        }
        if(Input.GetKey("r")){
                transform.position = starPos;
                ps.Clear();
        }
    }

    void Go(){    
        //StartCoroutine(MovePlayer());

        /*if(Input.GetKey("space")){

            //if(stopGo == true){ stopGo = false;}
            //else stopGo = true;
        }*/

        if(stopGo == false){
            if(i<ps.Count-1) i++;
            transform.Translate(ps[i].x, ps[i].y, 0);
        }
        
    }

    // Corutina
    /*
    //Esto es una corutina. Es como hacer otro hilo de ejecución (trampeando, realmente no lo haces)
    //Al hacer "yield return null" lo que haces es esperar al siguiente frame para continuar con la ejecución del código
    //Por lo tanto, si quieres hacer un efecto de Degradado de Color, lo harías de ésta manera para facilitarte la vida
    //La opinión de Victor es usarlas lo mínimo posible, ésto se podria hacer de otra manera, peeeero hacerlo con corutinas te facilita la vida
    //Si quisieras hacerlo sin corrutinas lo harias sin un bucle for, simplemente poner el i++ fuera del bucle y acceder a esa posición en cada frame
    IEnumerator MovePlayer() {
        for (i = 0; i < ps.Count; i++){
            transform.Translate(ps[i].x, ps[i].y, 0);
         yield return null;
        }
    }

    //Si quisieras parar la ejecución de una corutina, es un "pequeño" problema.
    //Tendrias que guardar en una variable qué objeto en concreto la ha iniciado (si sólo se va a llamar aquí no hace falta, pero si por ejemplo
    //la empiezas en otro scipt, entonces mejor tener guardado quién ha llamado a esa corutina)
    //Además tendrías que guardar en una variable la propia corutina
    
    IEnumerator newCor = MovePlayer();
        StartCoroutine(newCor);
        //StopCoroutine(newCor);
    */
}
